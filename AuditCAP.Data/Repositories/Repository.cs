﻿
using AuditCAP.Domain.Entities;
using AuditCAP.Domain.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;


namespace AuditCAP.Data.Repositories
{
    public class Repository<T> : IRepository<T> where T : BaseEntity
    {
        private readonly AuditCAPDbContext _context;

        private readonly ILogger<Repository<T>> _logger;

        public Repository(AuditCAPDbContext context,
            ILogger<Repository<T>> logger)
        {
            this._context = context;
            _logger = logger;

        }

        public async Task<bool> Add(T entity)
        {
            try
            {
                _logger.LogInformation($"Repository => Adding {nameof(entity)} with Id:  {entity.ID}");
                await _context.AddAsync(entity);
                var added = await _context.SaveChangesAsync();
                if (added == 0)
                {
                    _logger.LogError($"{nameof(entity)} could not be saved");
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                _logger.LogError($"{nameof(entity)} could not be saved: {ex.InnerException}");
                return false;
            }
        }

        public async Task<bool> Delete(T entity)
        {
            _logger.LogInformation($"Repository => Deleting Id: {entity.ID}");

            try
            {

                entity.Deleted = true;
                _context.Update<T>(entity);
                var deleted = await _context.SaveChangesAsync();
                if (deleted == 0)
                {
                    _logger.LogError($"{nameof(entity)} could not be deleted");
                    return false;
                }

                return true;
            }
            catch (Exception ex)
            {
                _logger.LogError($"{entity.ID} could not be deleted: {ex.InnerException}");
                return false;
            }
        }

        public IQueryable<T> GetAll(Func<T, bool> predicate = null)
        {
            _logger.LogInformation($"Repository => Getting All");

            try
            {
                var query = _context.Set<T>().Where(x => !x.Deleted);

                if (predicate != null)
                {
                    return query.Where(predicate).AsQueryable<T>();
                }
                return query;


            }
            catch (Exception ex)
            {
                _logger.LogError($"Couldn't retrieve entities: inner exception: {ex.InnerException} and errorMessage: {ex.Message}");
                return Enumerable.Empty<T>().AsQueryable();
            }
        }

        public IQueryable<T> GetAll(Expression<Func<T, bool>> predicate,
           string[] includes = null)
        {
            var query = _context.Set<T>().Where(predicate);
            foreach (var include in includes)
                query = query.Include(include);
            return query;
        }

        public async Task<T> GetById(object id)
        {
            _logger.LogInformation($"Repository => Getting entity by Id: {id}");
            try
            {
                return await _context.FindAsync<T>(id);

            }
            catch (Exception ex)
            {
                _logger.LogError($"Couldn't retrieve entitie: inner exception: {ex.InnerException} and errorMessage: {ex.Message}");
                return null;
            }

        }

        public async Task<T> GetByName(object name)
        {
            _logger.LogInformation($"Repository => Getting entity by name: {name}");

            try
            {
                return await _context.FindAsync<T>(name);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Couldn't retrieve entitie: inner exception: {ex.InnerException} and errorMessage: {ex.Message}");
                return null;
            }

        }

        public async Task<bool> Update(T entity)
        {
            _logger.LogInformation($"Repository => Updating entity {entity} with id : {entity.ID}");

            try
            {
                _context.Update<T>(entity);
                var updated = await _context.SaveChangesAsync();
                if (updated == 0)
                {
                    _logger.LogError($"{nameof(entity)} could not be updated");
                    return false;
                }

                return true;

            }
            catch (Exception ex)
            {
                _logger.LogError($"{nameof(entity)} could not be updated: {ex.InnerException}");
                return false;
            }
        }


    }
}
