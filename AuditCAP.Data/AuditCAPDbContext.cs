﻿using AuditCAP.Domain.Entities;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System;

namespace AuditCAP.Data
{
    public class AuditCAPDbContext : IdentityDbContext<AppUser>
    {
        public AuditCAPDbContext(DbContextOptions<AuditCAPDbContext> options) : base(options)
        {
            ChangeTracker.StateChanged += UpdateTimestamps;
            ChangeTracker.Tracked += UpdateTimestamps;
        }

        public virtual DbSet<QAChildSupportNonComplianceSanction> QAChildSupportNonComplianceSanction { get; set; }
      
        private static void UpdateTimestamps(object sender, EntityEntryEventArgs e)
        {
            if (e.Entry.Entity is BaseEntity entityWithTimestamps)
            {
                switch (e.Entry.State)
                {

                    case EntityState.Modified:
                        entityWithTimestamps.ModifiedDate = DateTime.UtcNow;
                        break;
                    case EntityState.Added:
                        entityWithTimestamps.ModifiedDate = DateTime.UtcNow;
                        entityWithTimestamps.AddedDate = DateTime.UtcNow;
                        break;
                }
            }
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.ApplyConfigurationsFromAssembly(typeof(QAChildSupportNonComplianceSanction).Assembly);


        }
    }
}
