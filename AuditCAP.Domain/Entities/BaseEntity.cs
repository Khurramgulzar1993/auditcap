﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuditCAP.Domain.Entities
{
    public abstract class BaseEntity
    {

        public Guid ID { get; set; }
        public DateTime AddedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
        public bool Deleted { get; set; }

    }
}
