﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Linq;

namespace AuditCAP.Domain.Entities
{
    public class AppUser : IdentityUser
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public bool Admin { get; set; }
        public bool Deleted { get; set; }
    }
}
