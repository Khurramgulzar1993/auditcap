﻿using AuditCAP.Domain.ViewModels.QAChildSupportNonComplianceSanction;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuditCAP.Domain.Dtos
{
    public class AddQAChildSupportNonComplianceSanction
    {
        public QAChildSupportNonComplianceSanctionViewModel QAChildSupportNonComplianceSanction {  get;set;}
    }
    
}
