﻿using AuditCAP.Domain.Entities;
using AuditCAP.Domain.ViewModels.QAChildSupportNonComplianceSanction;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuditCAP.Domain.Automapper
{
  public class QAChildSupportNonComplianceSanctionProfile:Profile
    {
        public QAChildSupportNonComplianceSanctionProfile() => CreateMap<QAChildSupportNonComplianceSanction, QAChildSupportNonComplianceSanctionViewModel>().ReverseMap();

    }
}
