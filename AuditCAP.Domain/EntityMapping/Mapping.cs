﻿using AuditCAP.Domain.Entities;
using Microsoft.EntityFrameworkCore;

using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace AuditCAP.Domain
{
    public class QAChildSupportNonComplianceSanctionMapping : IEntityTypeConfiguration<QAChildSupportNonComplianceSanction>
    {
        public void Configure(EntityTypeBuilder<QAChildSupportNonComplianceSanction> builder)
        {
            builder.HasKey(t => t.ID);
            builder.Property(t => t.ID).IsRequired();
            builder.Property(t => t.CaseNumber).IsRequired();
            builder.Property(t => t.CustomerName).HasMaxLength(50).IsRequired();
            builder.Property(t => t.IncomeSupport).IsRequired();
            builder.Property(t => t.NotedExpectation).IsRequired();
            builder.Property(t => t.ErrorType).IsRequired();
            builder.Property(t => t.WasthisUserDeficiency).IsRequired();
            builder.Property(t => t.TypeofError).IsRequired();
            builder.Property(t => t.OQAComments).IsRequired();
            builder.Property(t => t.DateofReviewbyQAMyProperty).IsRequired();
            builder.Property(t => t.WastheNoticeContentCorrect).IsRequired();
            builder.Property(t => t.WasTimelyNoticeIssuedtotheCustomer).IsRequired();
            builder.Property(t => t.TANFAllotmentReceivedPostSanction).IsRequired();
            builder.Property(t => t.TANFAllotmentReceivedDuringSanctionPeriod).IsRequired();
            builder.Property(t => t.SanctionEndDate).IsRequired();
            builder.Property(t => t.SanctionLifted).IsRequired();
            builder.Property(t => t.ActionTakeAt).IsRequired();
            builder.Property(t => t.Employee).IsRequired();
            builder.Property(t => t.CCCSRDate).IsRequired();
            builder.Property(t => t.DateRecieved).IsRequired();
            builder.Property(t => t.TypeofSanctionImposed).IsRequired();

        }

    }
}
