﻿using AuditCAP.Domain.Entities;
using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;


namespace AuditCAP.Domain.Repositories
{
    public interface IRepository<T> where T : BaseEntity
    {
        Task<T> GetById(object id);
        Task<bool> Add(T entity);
        Task<bool> Update(T entity);
        Task<bool> Delete(T entity);
        IQueryable<T> GetAll(Func<T, bool> predicate = null);
        IQueryable<T> GetAll(Expression<Func<T, bool>> predicate, string[] includes = null);
        Task<T> GetByName(object name);
        
    }
}
