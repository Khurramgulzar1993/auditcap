﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuditCAP.Domain.ViewModels.QAChildSupportNonComplianceSanction
{
   public class QAChildSupportNonComplianceSanctionViewModel
    {
        public int CaseNumber { get; set; }
        public int CustomerName { get; set; }

        public int IncomeSupport { get; set; }
        public int TypeofSanctionImposed { get; set; }
        public DateTimeOffset DateRecieved { get; set; }
        public DateTimeOffset CCCSRDate { get; set; }
        public string Employee { get; set; }
        public DateTimeOffset ActionTakeAt { get; set; }
        public int SanctionLifted { get; set; }
        public DateTimeOffset SanctionEndDate { get; set; }

        public int TANFAllotmentReceivedDuringSanctionPeriod { get; set; }
        public int TANFAllotmentReceivedPostSanction { get; set; }

        public int WasTimelyNoticeIssuedtotheCustomer { get; set; }
        public int WastheNoticeContentCorrect { get; set; }

        public DateTimeOffset DateofReviewbyQAMyProperty { get; set; }

        public string OQAComments { get; set; }

        public string ChildSupportComments { get; set; }
        public int TypeofError { get; set; }
        public int WasthisUserDeficiency { get; set; }

        public int ErrorType { get; set; }

        public int NotedExpectation { get; set; }
    }
}
