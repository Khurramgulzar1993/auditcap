﻿using AuditCAP.Domain.Entities;
using AuditCAP.Domain.Repositories;
using AuditCAP.Domain.ViewModels.QAChildSupportNonComplianceSanction;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AuditCAP.Controllers
{
    public class QAChildSupportNonComplianceSanctionController : Controller
    {

        private readonly IMapper _mapper;
        private readonly IRepository<QAChildSupportNonComplianceSanction> _qAChildSupportNonComplianceSanctionRepo;

        public QAChildSupportNonComplianceSanctionController(IMapper mapper)
        {
            _mapper = mapper;
        }


      
        public IActionResult Index()
        {
            return View();
        }


    }
}
